# VSPG Core
![alt text](https://framagit.org/uploads/-/system/group/avatar/43224/vspg-logo.png "Logo Very Simple Password Generator")

VSPG is an API is a beatiful REST API designed to generate "**Strong**" one time password.

The list of words is for the moment based on the French dictionary.


But you can also generate an *access_key* and *secret_key* pair

## Installation

### Easy way
Use Docker
```bash
docker pull justinbriard/vspg
```

### The Hard way but good way
Use git clone and run the binary as a service

```bash
git clone git@framagit.org:vspg/vspg_core.git
```

## Requirements

- Python 3.x
- Pip
  - bottle
  - paste
  - urllib3

## Start
```bash
python vspg-api.py
```



## Usage

**Body Json**

You need to send this body, if you omit any of the elements you will get an error.
```json
{
"number_of_words":"5",
"number_of_numbers":"2",
"separator":"0",
"language":"en"
}
```
You need to set this 4 elements :
- number_of_words
- number_of_numbers
- separator
- language

*number_of_words*
This is literraly the number of words you will get.

*number_of_numbers*
It's the number of digits you'll have in your number.

*separator*
- If set to 0 you will get only user frinedly separator from this list :  [-_:+?;!,]
- If set to 1 you will get anything present in *string.punctuation* : [!"#$%&'()*+, -./:;<=>?@[\]^_`{|}~]
- If set to 2 you will get separator from this list :  [-_]


If set to 1 you can get any ASCII characters which are considered punctuation characters in the C locale.

*language*
You can choose the dictionary from which the words will be chosen.
Valid options are
- **en** : more than 370000 English words
- **fr** : more than 330000 French words 
- **fr_simple** : more than 22000 French words 
- **it** : more than 88300 Italian words
- **de** : more than 166100 German words
- **es** : more than 174800 Spanish words


**Endpoint v1/elements**

```bash
curl --request POST \
  --url http://[server]:[port]/v1/elements \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"5",
"number_of_numbers":"2",
"separator":"0",
"language":"fr_simple"
}'
```
result:
```json
{
  "status": "ok",
  "result": [
    ",",
    "07",
    "dissoute",
    "SNECMA"
  ]
}
```


**Endpoint v1/password**

```bash
curl --request POST \
  --url http://[server]:[port]/v1/password \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"2",
"number_of_numbers":"4",
"separator":"1",
"language":"fr_simple"
}'
```
result:
```json
{
  "status": "ok",
  "result": "MartialLudwigSubie[3048"
}
```

**Endpoint v1/access_key**

```bash
curl --request GET \
  --url http://[server]:[port]/v1/access_key
```
result:
```json
{
    "status": "ok",
    "result": [
        {
            "access_key": "PFP5NIM1IJI69JZYCDDD",
            "secret_key": "LqIDaoFWuF0IHGuHpJXjXOCz42FScim3pfX7XzRf"
        }
    ]
}
```

**Endpoint v2/elements**

```bash
curl --request POST \
  --url http://[server]:[port]/v2/elements \
  --header 'content-type: application/json' \
  --data '{
"number_of_words":"5",
"number_of_numbers":"2",
"separator":"0",
"language":"fr_simple"
}'
```
result:
```json
{
  "status": "ok",
  "result": [
    {
      "separator": ":",
      "number": "85",
      "words": [
        "fusion",
        "Bebes",
        "conjoints",
        "petite",
        "analytiquement"
      ]
    }
  ]
}
```

**Endpoint /checkhealth**

```bash
curl --request GET \
  --url http://[server]:[port]/checkhealth
```
result:
```json
{
    "status": "ok"
}
```

**Endpoint /version**

```bash
curl --request GET \
  --url http://[server]:[port]/version
```
result:
```text
1.4
```

## Idea
The idea is comming from [XKCD](https://xkcd.com/936/) and the [ANSSI](https://www.ssi.gouv.fr/)

## Demo
You can see more info on https://vspg.otbm.fr/ or test the API https://api.vspg.otbm.fr

## Sources of dictionaries

- English : https://github.com/dwyl/english-words
- French : http://www.pallier.org/liste-de-mots-francais.html (heavily modified by me)
- French simple : https://www.freelang.com/dictionnaire/dic-francais.php (heavily modified by me)
- German, Italian and Spanish : http://www.gwicks.net/dictionaries.htm

## Project status
Under development

