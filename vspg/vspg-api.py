#!/usr/bin/python3

from bottle import route, run, request, response, HTTPError, error, install, view
import random
import string
import json
import urllib3, logging
import requests
import sys
from secrets import choice
import hashlib

class out:

    INFO, WARN, ERROR = ((32, "INFO"), (33, "WARN"), (31, "ERROR"))

    @staticmethod
    def _output(message, type):
        sys.stdout.write("[\033[1;%im%s\033[0;0m] " % type + "%s\n" % message)

    @staticmethod
    def info(message):
        out._output(message, out.INFO)

    @staticmethod
    def warn(message):
        out._output(message, out.WARN)

    @staticmethod
    def error(message):
        out._output(message, out.ERROR)
        sys.exit(1)


@route("/", method="GET")
@view("index.tpl")
def index() :
    url = request.headers['HOST']
    try:

        response.status = 200

    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}

    return {"url": url}

@route("/v1/elements", method="POST")
def getElements():
    """

    """
    try:
        postdata = request.body.read()
        response.add_header("X-VSPG", "true")
        ret = generatElements(postdata)
        response.status = 200
        return {"status": "ok", "result": ret}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/v1/password", method="POST")
def getPassword():
    """

    
    """
    try:
        postdata = request.body.read()
        ret = generatePassword(postdata)
        response.add_header("X-VSPG", "true")
        sha_signature = encrypt_string(ret)
        response.add_header("X-VSPG-sha256", sha_signature)
        response.status = 200
        return {"status": "ok", "result": ret}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/v1/access_key", method="GET")
def getAccessKey():
    """

    """
    try:
        response.add_header("X-VSPG", "true")
        ret = generateAccessKey()
        response.status = 200
        return {"status": "ok", "result": ret}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/v2/elements", method="POST")
def getElements():
    """

    """
    try:
        response.add_header("X-VSPG", "true")
        postdata = request.body.read()
        ret = generatElementsV2(postdata)
        response.status = 200
        return {"status": "ok", "result": [ret]}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}
        
        
@route("/checkhealth", method="GET")
def get_checkhealth():
    try:
        response.status = 200
        return {"status": "ok"}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}

@route("/version", method="GET")
def get_version():
    try:
        response.status = 200
        return version
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@error(404)
def error404(error):
    return {"status": "error", "error": "not found"}


def random_line(fname):
    lines = open(fname).read().splitlines()
    return random.choice(lines)


def get_language_file(language):
    if language == "fr":
        file_word = "dictionnaires/liste_francais.txt"
    elif language == "en":
        file_word = "dictionnaires/liste_anglais.txt"
    elif language == "it":
        file_word = "dictionnaires/liste_italien.txt"
    elif language == "es":
        file_word = "dictionnaires/liste_espagnole.txt"
    elif language == "de":
        file_word = "dictionnaires/liste_allemand.txt"
    elif language == "fr_simple":
        file_word = "dictionnaires/liste_francais_simple.txt"
    else:
        file_word = "dictionnaires/liste_francais_simple.txt"

    return file_word


def generatElements(postdata):
    ret = {}
    number_of_words = 5
    payload = json.loads(postdata)
    result_list = []
    number_of_numbers = int(payload["number_of_numbers"])
    friendly_separator = int(payload["separator"])
    number_of_words = int(payload["number_of_words"])
    language = payload["language"]
    file_word = get_language_file(language)

    try:
        # generate x numbers
        numbers_a = generateNumbers(number_of_numbers)
        # create an array with the numbers
        numbers_s = "".join(map(str, numbers_a))

        if friendly_separator == 1:
            separator = random.choice(string.punctuation)
        elif friendly_separator == 2:
            separators_list = ["-", "_"]
            separator = random.choice(separators_list)
        else:
            separators_list = ["-", "_", ":", "+", "?", ";", "!", ","]
            separator = random.choice(separators_list)

        if number_of_words == 0:
            liste_word =""
        else:
            liste_word = generateWordsList(file_word, number_of_words)
        
        # Build the result table
        result_list = [separator, numbers_s]
        result_list.extend(liste_word)

        ret = result_list

    except Exception as e:
        raise Exception("Something Bad append generatElements - %s" % e)
    return ret

def generatElementsV2(postdata):
    ret = {}
    number_of_words = 5
    payload = json.loads(postdata)
    result_list = []
    number_of_numbers = int(payload["number_of_numbers"])
    friendly_separator = int(payload["separator"])
    number_of_words = int(payload["number_of_words"])
    language = payload["language"]
    file_word = get_language_file(language)

    try:
        # generate x numbers
        numbers_a = generateNumbers(number_of_numbers)
        # create an array with the numbers
        numbers_s = "".join(map(str, numbers_a))

        if friendly_separator == 1:
            separator = random.choice(string.punctuation)
        elif friendly_separator == 2:
            separators_list = ["-", "_"]
            separator = random.choice(separators_list)
        else:
            separators_list = ["-", "_", ":", "+", "?", ";", "!", ","]
            separator = random.choice(separators_list)

        if number_of_words == 0:
            liste_word =""
        else:
            liste_word = generateWordsList(file_word, number_of_words)

        # Build the result table
        result_list = {
               "separator": "",
               "number": "",
               "words": "",
           }
        result_list["separator"] = separator
        result_list["number"] = numbers_s
        result_list["words"] = liste_word

        ret = result_list

    except Exception as e:
        raise Exception("Something Bad append generatElements - %s" % e)
    return ret
    
    
def generateAccessKey():
    ret = []
    try:
        access_key = generateKey("u", 20)
        secret_key = generateKey("l", 40)

        keys = {"access_key": "", "secret_key": ""}
        keys["access_key"] = access_key
        keys["secret_key"] = secret_key
        ret.append(keys)

    except Exception as e:
        raise Exception("Something Bad append generateAccessKey - %s" % e)
    return ret


def generateKey(case, lenght):
    try:
        liste = string.ascii_letters + string.digits
        if case == "u":
            ret = "".join(choice(liste) for i in range(lenght))
            ret = ret.upper()
        elif case == "l":
            ret = "".join(choice(liste) for i in range(lenght))

    except Exception as e:
        raise Exception("Something Bad append generateKey- %s" % e)
    return ret


def generateWordsList(file_word, number_of_words):
    number_of_word_i = 0
    liste_word = []
    try:
        while number_of_word_i < number_of_words:
            name_rand = random_line(file_word)
            liste_word.append(name_rand)
            number_of_word_i += 1
            passwordWords = liste_word
    except Exception as e:
        raise Exception("Something Bad append generateWordsList - %s" % e)
    return passwordWords




def generateNumbers(number_of_numbers):
    try:
        passwordNumbers = "".join(
            choice(string.digits) for i in range(number_of_numbers)
        )
    except Exception as e:
        raise Exception("Something Bad append generateNumbers - %s" % e)
    return passwordNumbers


def generatePassword(postdata):
    mdp = ""
    try:
        url = "http://localhost:%s/v1/elements" % port
        payload = json.loads(postdata)
        headers = {"content-type": "application/json"}
        response = requests.request("POST", url, json=payload, headers=headers)
        body = json.loads(response.text)
        result = body["result"]

        for i in range(2, len(result)):
            mdp += result[i].capitalize()
        mdp += result[0] + result[1]

        ret = mdp
    except Exception as e:
        raise Exception("Something Bad append generatePassword - %s" % e)
    return ret


def encrypt_string(hash_string):
        sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
        return sha_signature

def main():
    out.info("Starting VSPG API")

    try:
        with open("vspg.conf") as f:
            c = json.load(f)
    except IOError:
        out.error("Could not read vspg.conf file (missing? permission?)")
    except ValueError:
        out.error("Could not parse vspg.conf file (not valid json?)")

    try:
        global port
        port = c["http"]["port"]
    except Exception as e:
        out.error("Could not find %s on vspg.conf (missing? out of scope ?)" % e)

    try:
        host = c["http"]["host"]
    except Exception as e:
        out.error("Could not find %s on vspg.conf (missing? out of scope ?)" % e)

    try:
        global version
        version = c["server"]["version"]
    except Exception as e:
        out.error("Could not find %s on vspg.conf (missing? out of scope ?)" % e)


    out.info("Configuration file loaded")

    out.info("Starting http server")

    logging.getLogger("wsgi").addHandler(logging.NullHandler())
    run(
        host=host,
        port=port,
        quiet=True,
        server="paste",
        use_threadpool=True,
        threadpool_workers=15,
        request_queue_size=5,
    )


if __name__ == "__main__":
    main()
